/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entitys;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author smart
 */
@Entity
@Table(name = "clientes_tbl")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ClientesTbl.findAll", query = "SELECT c FROM ClientesTbl c"),
    @NamedQuery(name = "ClientesTbl.findByCedula", query = "SELECT c FROM ClientesTbl c WHERE c.cedula = :cedula"),
    @NamedQuery(name = "ClientesTbl.findByPriNombre", query = "SELECT c FROM ClientesTbl c WHERE c.priNombre = :priNombre"),
    @NamedQuery(name = "ClientesTbl.findBySegNombre", query = "SELECT c FROM ClientesTbl c WHERE c.segNombre = :segNombre"),
    @NamedQuery(name = "ClientesTbl.findByPriApellido", query = "SELECT c FROM ClientesTbl c WHERE c.priApellido = :priApellido"),
    @NamedQuery(name = "ClientesTbl.findBySegApellido", query = "SELECT c FROM ClientesTbl c WHERE c.segApellido = :segApellido"),
    @NamedQuery(name = "ClientesTbl.findByDireccion", query = "SELECT c FROM ClientesTbl c WHERE c.direccion = :direccion"),
    @NamedQuery(name = "ClientesTbl.findByTelefono", query = "SELECT c FROM ClientesTbl c WHERE c.telefono = :telefono"),
    @NamedQuery(name = "ClientesTbl.findByMovil", query = "SELECT c FROM ClientesTbl c WHERE c.movil = :movil"),
    @NamedQuery(name = "ClientesTbl.findByEmail", query = "SELECT c FROM ClientesTbl c WHERE c.email = :email"),
    @NamedQuery(name = "ClientesTbl.findByVehiculo", query = "SELECT c FROM ClientesTbl c WHERE c.vehiculo = :vehiculo"),
    @NamedQuery(name = "ClientesTbl.findByVivienda", query = "SELECT c FROM ClientesTbl c WHERE c.vivienda = :vivienda"),
    @NamedQuery(name = "ClientesTbl.findByProfesion", query = "SELECT c FROM ClientesTbl c WHERE c.profesion = :profesion"),
    @NamedQuery(name = "ClientesTbl.findByFechaNacimiento", query = "SELECT c FROM ClientesTbl c WHERE c.fechaNacimiento = :fechaNacimiento"),
    @NamedQuery(name = "ClientesTbl.findByFormalaboral", query = "SELECT c FROM ClientesTbl c WHERE c.formalaboral = :formalaboral")})
public class ClientesTbl implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "cedula")
    private Integer cedula;
    @Size(max = 20)
    @Column(name = "pri_nombre")
    private String priNombre;
    @Size(max = 20)
    @Column(name = "seg_nombre")
    private String segNombre;
    @Size(max = 20)
    @Column(name = "pri_apellido")
    private String priApellido;
    @Size(max = 20)
    @Column(name = "seg_apellido")
    private String segApellido;
    @Size(max = 35)
    @Column(name = "direccion")
    private String direccion;
    @Size(max = 25)
    @Column(name = "telefono")
    private String telefono;
    @Size(max = 25)
    @Column(name = "movil")
    private String movil;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 40)
    @Column(name = "email")
    private String email;
    @Column(name = "vehiculo")
    private Boolean vehiculo;
    @Column(name = "vivienda")
    private Boolean vivienda;
    @Size(max = 30)
    @Column(name = "profesion")
    private String profesion;
    @Column(name = "fecha_nacimiento")
    @Temporal(TemporalType.DATE)
    private Date fechaNacimiento;
    @Size(max = 25)
    @Column(name = "formalaboral")
    private String formalaboral;

    public ClientesTbl() {
    }

    public ClientesTbl(Integer cedula) {
        this.cedula = cedula;
    }

    public Integer getCedula() {
        return cedula;
    }

    public void setCedula(Integer cedula) {
        this.cedula = cedula;
    }

    public String getPriNombre() {
        return priNombre;
    }

    public void setPriNombre(String priNombre) {
        this.priNombre = priNombre;
    }

    public String getSegNombre() {
        return segNombre;
    }

    public void setSegNombre(String segNombre) {
        this.segNombre = segNombre;
    }

    public String getPriApellido() {
        return priApellido;
    }

    public void setPriApellido(String priApellido) {
        this.priApellido = priApellido;
    }

    public String getSegApellido() {
        return segApellido;
    }

    public void setSegApellido(String segApellido) {
        this.segApellido = segApellido;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getMovil() {
        return movil;
    }

    public void setMovil(String movil) {
        this.movil = movil;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getVehiculo() {
        return vehiculo;
    }

    public void setVehiculo(Boolean vehiculo) {
        this.vehiculo = vehiculo;
    }

    public Boolean getVivienda() {
        return vivienda;
    }

    public void setVivienda(Boolean vivienda) {
        this.vivienda = vivienda;
    }

    public String getProfesion() {
        return profesion;
    }

    public void setProfesion(String profesion) {
        this.profesion = profesion;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getFormalaboral() {
        return formalaboral;
    }

    public void setFormalaboral(String formalaboral) {
        this.formalaboral = formalaboral;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cedula != null ? cedula.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ClientesTbl)) {
            return false;
        }
        ClientesTbl other = (ClientesTbl) object;
        if ((this.cedula == null && other.cedula != null) || (this.cedula != null && !this.cedula.equals(other.cedula))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entitys.ClientesTbl[ cedula=" + cedula + " ]";
    }
    
}
