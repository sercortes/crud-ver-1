/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import entitys.ClientesTbl;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
//import javax.enterprise.context.Dependent;
import modelo.ClientesTblFacade;

/**
 *
 * @author smart
 */
@ManagedBean(name = "clientecontrolador")
@SessionScoped
public class clientecontrolador {

    /**
     * Creates a new instance of clientecontrolador
     */
      @EJB
    private ClientesTblFacade clientesfacade;
    private ClientesTbl clientestbl=new ClientesTbl();

    public ClientesTbl getClientestbl() {
        return clientestbl;
    }

    public void setClientestbl(ClientesTbl clientestbl) {
        this.clientestbl = clientestbl;
    }
    
      
    public List<ClientesTbl> findAll(){
     return this.clientesfacade.findAll();
    }

    public String insert(){
    this.clientesfacade.create(clientestbl);
    this.clientestbl= new ClientesTbl();
    return "index";
    }
    
    public void eliminar(ClientesTbl clientestbl){
        this.clientesfacade.remove(clientestbl);
        
    }
    public String actualizar(ClientesTbl clientestbl){
        this.clientestbl=clientestbl;
        return "actulizar";
    }
    public String actualizar(){
    this.clientesfacade.edit(clientestbl);
    this.clientestbl= new ClientesTbl();
    return "index";
    }
    
    public clientecontrolador() {
    }
    
}
