-- MySQL dump 10.13  Distrib 5.7.16, for Linux (x86_64)
--
-- Host: localhost    Database: sgc
-- ------------------------------------------------------
-- Server version	5.7.16-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `clientes_tbl`
--

DROP TABLE IF EXISTS `clientes_tbl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clientes_tbl` (
  `cedula` int(15) NOT NULL,
  `pri_nombre` varchar(20) DEFAULT NULL,
  `seg_nombre` varchar(20) DEFAULT NULL,
  `pri_apellido` varchar(20) DEFAULT NULL,
  `seg_apellido` varchar(20) DEFAULT NULL,
  `direccion` varchar(35) DEFAULT NULL,
  `telefono` varchar(25) DEFAULT NULL,
  `movil` varchar(25) DEFAULT NULL,
  `email` varchar(40) DEFAULT NULL,
  `vehiculo` tinyint(1) DEFAULT NULL,
  `vivienda` tinyint(1) DEFAULT NULL,
  `profesion` varchar(30) DEFAULT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  `formalaboral` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`cedula`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clientes_tbl`
--

LOCK TABLES `clientes_tbl` WRITE;
/*!40000 ALTER TABLE `clientes_tbl` DISABLE KEYS */;
INSERT INTO `clientes_tbl` VALUES (465445,'Jorge','Alfredo','Rodriguez','Sifuentes','calle 45 A sur','3123555689','7252428','Alfredo@gmail.com',1,0,'Contador',NULL,'Contrato Laboral'),(104258752,'luz','Maria','Rojas','Cubidez','calle 63 # 23-67','7242526','3205874586','luzrojas@gmail.com',0,0,'Doctora',NULL,'Contrato');
/*!40000 ALTER TABLE `clientes_tbl` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-11-04 22:48:57
